const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
var bodyParser = require('body-parser');
var route = require("./route");
var restful = require('node-restful');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const uri = "mongodb+srv://fin:asrkpvg7@cluster0-ogucd.mongodb.net/work?retryWrites=true&w=majority";
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");
    // mongoose.close();
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("h");
});

var app = express();
// app.use(express.static(path.join(__dirname, 'public')));
app.use('/static', express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')
app.get("/", (req, res)=>{
    res.render("note/note");
})
app.get("/new", (req, res)=>{
    res.render("note/new");
})
app.get("/all", (req, res)=>{
    res.render("note/all");
})
require('./app/routes/note.routes.js')(app);
app.listen(PORT);