var mongo = require("./mongodb");
function mainPage(req, res){
    res.render("order/all");
}
function customer(req, res){
    res.render("creta/new_customer");
}

function createCustomer(req, res){
    var a = req.body;
    a["id"] = (new Date()).getTime();
    mongo.insert("creta", "customers", a, function(r){
        res.send({
            status: "OK",
            data: "NONE"
        });
    })
}

function acustomer(req, res){
    mongo.find("creta", "customers", {}, function(r){
        res.send({
            status: "OK",
            data: r
        })
    })
}

function anote(req, res){
    var a = req.body;
    console.log("A", a);
    var note = {};
    note["note-" + (new Date()).getTime().toString()] = a.note;
    console.log(note);
    mongo.rowupdate("creta", "customers", {
        $push: {
            note: {
                staff: 0,
                note: a.note,
                date: (new Date()).getTime()
            }
        }
    }, {id: a.obj.id}).then((a)=>{
        console.log("SUCCESS");
        res.send({
            "status": "OK",
            // data: a
        })
    }).catch((e)=>{
        res.send({
            "status": "ERROR",
            // data: e
        })
    })
}

function edelete(req, res){
    var a = req.body;
    console.log(a);
    mongo.delete("creta", "customers", {id: a.id}, function(a){
        res.send({
            status: "OK",
            data: a
        })
    })
}

function cus(req, res){
    var a = req.params;
    console.log(a);
}

function show(req, res){
    res.render("body/show", {
        page: "../" + req.params.page + "/" + req.params.page 
    });
}

function corder(req, res){
    var a = req.query;
    switch(a.page){
        case "all":{
            res.render("order/index", {
                page: "all"
            })
        } break;
        case "new":{
            res.render("order/index", {
                page: "create"
            })
        }break;
        default:{
            res.send({
                status: "ERROR",
                data: "CANNOT FOUND CONTROL WITH APP"
            });
        }
    }

}

function cimpo(req, res){
    res.render("impo/index")
}

function app(req, res){
    var a = req.params;
    console.log(a);
    switch(a.func){
        case "find_order":{
            res.render("app/dashboard_order")
        } break;
        default:{
            res.render("app/dashboard");
        }
    }
}

exports.test = (req, res) =>{
    res.render("test")
}

module.exports.mainPage = mainPage;
module.exports.createCustomer = createCustomer;
module.exports.customer = customer;
module.exports.acustomer = acustomer;
module.exports.anote = anote;
module.exports.delete = edelete;
module.exports.cus = cus;
module.exports.show = show;
module.exports.corder = corder;
module.exports.cimpo = cimpo;
module.exports.app = app;